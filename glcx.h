#include <gl/gl.h>
#include <string>
#include "glc.h"

namespace glcx {

// glcx::BoxParameters

  class BoxParameters: public glc::ObjectParameters {
  public:
    float width, height, length;
    glc::Color colors[6];
  };

// glcx::Box

  class Box: public glc::Object {
  public:
    enum {
      FRONT, BACK, LEFT, RIGHT, TOP, BOTTOM
    };

    float width, height, length;
    glc::Color colors[6];
    float faces[6][4];
    
    Box(BoxParameters *parameters): Object((glc::ObjectParameters*)parameters) {
      width = parameters->width / 2.0f;
      height = parameters->height / 2.0f;
      length = parameters->length / 2.0f;
      for (int i = 0; i < 6; ++i) colors[i] = parameters->colors[i];
    }

    virtual void render() {
#define C(a) glColor3f(colors[a].r, colors[a].g, colors[a].b); glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, (GLfloat*)&colors[a])
#define V(a, b, c) glVertex3f(a width, b height, c length)
      glBegin(GL_QUADS);
        glNormal3f(0.0f, 0.0f, 1.0f);  C(0); V(+,-,+); V(+,+,+); V(-,+,+); V(-,-,+);
        glNormal3f(0.0f, 0.0f, -1.0f); C(1); V(+,-,-); V(-,-,-); V(-,+,-); V(+,+,-);
        glNormal3f(-1.0f, 0.0f, 0.0f);  C(2); V(-,-,-); V(-,-,+); V(-,+,+); V(-,+,-);
        glNormal3f(1.0f, 0.0f, 0.0f); C(3); V(+,-,-); V(+,+,-); V(+,+,+); V(+,-,+);
        glNormal3f(0.0f, 1.0f, 0.0f);  C(4); V(-,+,-); V(-,+,+); V(+,+,+); V(+,+,-);
        glNormal3f(0.0f, -1.0f, 0.0f); C(5); V(-,-,-); V(+,-,-); V(+,-,+); V(-,-,+);
      glEnd();
#undef V
#undef C
    }

    virtual void renderForSelect() {
#define V(a, b, c) glVertex3f(a width, b height, c length)
      glBegin(GL_QUADS);
        glColor3ub(id >> 16, (id >> 8) & 255, id & 255);
        V(+,-,+); V(+,+,+); V(-,+,+); V(-,-,+);
        V(+,-,-); V(-,-,-); V(-,+,-); V(+,+,-);
        V(-,-,-); V(-,-,+); V(-,+,+); V(-,+,-);
        V(+,-,-); V(+,+,-); V(+,+,+); V(+,-,+);
        V(-,+,-); V(-,+,+); V(+,+,+); V(+,+,-);
        V(-,-,-); V(+,-,-); V(+,-,+); V(-,-,+);
      glEnd();
#undef V
    }
  };

  typedef void (*MenuFunc)(glc::Id MenuItemId);

  class MenuItemParameters: public glc::ObjectParameters {
  public:
    std::string caption;
    MenuFunc action;
  };

  class MenuItem: public glc::Object {
  public:
    std::string caption;
    MenuFunc action;

    MenuItem(MenuItemParameters* parameters): Object((glc::ObjectParameters*)parameters) {
      caption = parameters->caption;
      action = parameters->action;
    }

    void render() {
//  int window = glutGetWindow () ;
      glDisable ( GL_DEPTH_TEST );
      glMatrixMode ( GL_PROJECTION );
      glPushMatrix();
      glLoadIdentity();
      glOrtho(0, glutGet ( GLUT_WINDOW_WIDTH ), 
              0, glutGet ( GLUT_WINDOW_HEIGHT ), -1, 1 );
      glMatrixMode ( GL_MODELVIEW );
      glPushMatrix ();
      glLoadIdentity ();
      glColor3ub ( 0, 0, 0 );
      glRasterPos2i ( 10, glutGet ( GLUT_WINDOW_HEIGHT ) - 10 );
      glutBitmapString(GLUT_BITMAP_HELVETICA_12, (unsigned char*)caption.c_str());
      glPopMatrix();
      glMatrixMode ( GL_PROJECTION );
      glPopMatrix();
    }

    void renderForSelect() {
    }
  };

  class Menu: public glc::ComplexObject {
  public:
    void render() {
      
    }
  };

}

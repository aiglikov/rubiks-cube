#ifndef GLC_H_INCLUDED
#define GLC_H_INCLUDED

#include <map>

#include <stdio.h>

namespace glc {

  typedef int Id;

  Id id = 10;

  int genId() {
    return id++;
  }

  class Point {
  public:
    float x, y, z;
    Point() {
      x = y = z = 0.0f;
    }
    Point(float x, float y, float z) {
      this->x = x; this->y = y; this->z = z;
    }
    Point(Point *p) {
      x = p->x; y = p->y; z = p->z;
    }
  };

  class Color {
  public:
    float r, g, b, a;
    Color() {
      r = g = b = a = 1.0;
    }
    Color(float r, float g, float b, float a) {
      this->r = r; this->g = g; this->b = b; this->a = a;
    }
  };

// glc::ObjectParameters

  class ObjectParameters {
  public:
    Id id;
    Point position;
  };

// glc::ComplexObjectParameters

  class ComplexObjectParameters: public ObjectParameters {
  };

// glc::Object

  class Object {
  public:

    Id id;
    Point position;
    
    Object(ObjectParameters *parameters) {
      id = parameters->id;
      position = parameters->position;
    }

    virtual void render() = 0;

    virtual void renderForSelect() = 0;

  };

// glc::ComplexObject

  typedef std::map<Id, Object*> ObjectSet;

  class ComplexObject: public Object {
  public:

    ObjectSet subobjects;

    ComplexObject(ComplexObjectParameters *parameters): Object((ObjectParameters*)parameters) {
    }

    void render() {
      for (ObjectSet::iterator i = subobjects.begin(); i != subobjects.end(); ++i) {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(-i->second->position.x, -i->second->position.y, -i->second->position.z);
        i->second->render();
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
      }
    }

    void renderForSelect() {
      for (ObjectSet::iterator i = subobjects.begin(); i != subobjects.end(); ++i) {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glTranslatef(-i->second->position.x, -i->second->position.y, -i->second->position.z);
        i->second->renderForSelect();
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
      }
    }

    void addObject(Object *object) {
      subobjects[object->id] = object;
    }

    void removeObject(Object *object) {
      subobjects.erase(object->id);
    }
  };

  class ColorBuffer {
    ObjectSet objects;

  public:

    void clear() {
      objects.clear();
    }

    void addObject(Object *object) {
      objects[object->id] = object;
    }

    void removeObject(Object * object) {
      objects.erase(object->id);
    }

    Object* getObject(unsigned char red, unsigned char green, unsigned char blue) {
      Object* object = objects[(red << 16) + (green << 8) + blue];
      return object;
    }
  };
}

#endif

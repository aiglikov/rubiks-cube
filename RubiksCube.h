#include <vector>
#include <map>
#include "glcx.h"

class RubiksCubeParameters: public glc::ObjectParameters {
public:
  float size;
  bool shuffle;
};

class RubiksCube: public glc::ComplexObject {
public:
  
  float size;
  std::vector<glc::Id> front;
  std::vector<glc::Id> back;
  std::vector<glc::Id> left;
  std::vector<glc::Id> right;
  std::vector<glc::Id> top;
  std::vector<glc::Id> bottom;
  int all[3][3][3];
  glc::Point v[6];
  std::map<glc::Id, int> face;

  int rotation;
  float alpha, beta;
  int shuffling;

  glc::Color colors[7];

  RubiksCube(RubiksCubeParameters *parameters): ComplexObject((glc::ComplexObjectParameters*)parameters) {
    size = parameters->size;
    rotation = -1;
    alpha = beta = 0.0;
    shuffling = 0;

    colors[glcx::Box::FRONT].r = 1.0; colors[glcx::Box::FRONT].g = 0.0; colors[glcx::Box::FRONT].b = 0.0;
    colors[glcx::Box::BACK].r = 0.0; colors[glcx::Box::BACK].g = 1.0; colors[glcx::Box::BACK].b = 0.0;
    colors[glcx::Box::LEFT].r = 0.0; colors[glcx::Box::LEFT].g = 0.0; colors[glcx::Box::LEFT].b = 1.0;
    colors[glcx::Box::RIGHT].r = 1.0; colors[glcx::Box::RIGHT].g = 0.0; colors[glcx::Box::RIGHT].b = 1.0;
    colors[glcx::Box::TOP].r = 1.0; colors[glcx::Box::TOP].g = 1.0; colors[glcx::Box::TOP].b = 0.0;
    colors[glcx::Box::BOTTOM].r = 1.0; colors[glcx::Box::BOTTOM].g = 1.0; colors[glcx::Box::BOTTOM].b = 1.0;
    colors[6].r = 0.0; colors[6].g = 0.0; colors[6].b = 0.0;

    glcx::BoxParameters bp;
    bp.width = bp.height = bp.length = size / 3.0 - 0.01;

    v[glcx::Box::FRONT].x = 0.0; v[glcx::Box::FRONT].y = 0.0; v[glcx::Box::FRONT].z = -1.0; 
    v[glcx::Box::BACK].x = 0.0; v[glcx::Box::BACK].y = 0.0; v[glcx::Box::BACK].z = 1.0; 
    v[glcx::Box::LEFT].x = 1.0; v[glcx::Box::LEFT].y = 0.0; v[glcx::Box::LEFT].z = 0.0; 
    v[glcx::Box::RIGHT].x = -1.0; v[glcx::Box::RIGHT].y = 0.0; v[glcx::Box::RIGHT].z = 0.0; 
    v[glcx::Box::TOP].x = 0.0; v[glcx::Box::TOP].y = 1.0; v[glcx::Box::TOP].z = 0.0; 
    v[glcx::Box::BOTTOM].x = 0.0; v[glcx::Box::BOTTOM].y = -1.0; v[glcx::Box::BOTTOM].z = 0.0; 

    for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) for (int k = 0; k < 3; ++k)
      if ((i != 1) || (j != 1) || (k != 1)) {
        all[i][j][k] = bp.id = glc::genId();
        bp.position.x = (i - 1) * size / 3;
        bp.position.y = (j - 1) * size / 3;
        bp.position.z = (k - 1) * size / 3;
        for (int t = 0; t < 6; ++t) bp.colors[t] = colors[6];
        if (k == 0) {
          front.push_back(bp.id);
          face[bp.id] |= 1;
          bp.colors[glcx::Box::FRONT] = colors[glcx::Box::FRONT];
        }
        if (k == 2) {
          back.push_back(bp.id);
          face[bp.id] |= 2;
          bp.colors[glcx::Box::BACK] = colors[glcx::Box::BACK];
        }
        if (i == 2) {
          left.push_back(bp.id);
          face[bp.id] |= 4;
          bp.colors[glcx::Box::LEFT] = colors[glcx::Box::LEFT];
        }
        if (i == 0) {
          right.push_back(bp.id);
          face[bp.id] |= 8;
          bp.colors[glcx::Box::RIGHT] = colors[glcx::Box::RIGHT];
        }
        if (j == 0) {
          top.push_back(bp.id);
          face[bp.id] |= 16;
          bp.colors[glcx::Box::TOP] = colors[glcx::Box::TOP];
        }
        if (j == 2) {
          bottom.push_back(bp.id);
          face[bp.id] |= 32;
          bp.colors[glcx::Box::BOTTOM] = colors[glcx::Box::BOTTOM];
        }
        glcx::Box *cube = new glcx::Box(&bp);
        addObject(cube);
      }
  }

  void click(glc::Id objectId) {
    if (rotation != -1) return;
    std::vector<int> *s = 0;
    if (all[1][1][0] == objectId) {
      printf("front\n");
      s = &front;
      rotation = 0;
    } else if (all[1][1][2] == objectId) {
      printf("back\n");
      s = &back;
      rotation = 1;
    } else if (all[2][1][1] == objectId) {
      printf("left\n");
      s = &left;
      rotation = 2;
    } else if (all[0][1][1] == objectId) {
      printf("right\n");
      s = &right;
      rotation = 3;
    } else if (all[1][0][1] == objectId) {
      printf("top\n");
      s = &top;
      rotation = 4;
    } else if (all[1][2][1] == objectId) {
      printf("bottom\n");
      s = &bottom;
      rotation = 5;
    } else return;
    beta += 90.0;
    beta = (beta / 360.0 - floor(beta / 360.0)) * 360.0;
    for (std::vector<int>::iterator i = s->begin(); i != s->end(); ++i) {
      glcx::Box *cube = (glcx::Box*)subobjects[*i];
      for (int j = 0; j < 6; ++j) {
        cube->colors[j].a = 0.5;
      }
    }
  }

  void motion(glc::Id objectId) {
    for (glc::ObjectSet::iterator i = subobjects.begin(); i != subobjects.end(); ++i) {
      glcx::Box *cube = (glcx::Box*)i->second;
      if (i->second->id == objectId) {
        for (int j = 0; j < 6; ++j) {
          cube->colors[j].a = 0.5;
        }
      } else {
        for (int j = 0; j < 6; ++j) {
          cube->colors[j].a = 1.0;
        }
      }
    }
  }

  void rotate() {
    glc::Color a[3][3][6];
    switch (rotation) {
      case glcx::Box::FRONT:
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[2 - j][i][0]];
          for (int t = 0; t < 6; ++t) a[i][j][t] = cube->colors[t];
        }
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[i][j][0]];
          cube->colors[glcx::Box::FRONT] = a[i][j][glcx::Box::FRONT];
          cube->colors[glcx::Box::BACK] = a[i][j][glcx::Box::BACK];
          cube->colors[glcx::Box::TOP] = a[i][j][glcx::Box::LEFT];
          cube->colors[glcx::Box::LEFT] = a[i][j][glcx::Box::BOTTOM];
          cube->colors[glcx::Box::BOTTOM] = a[i][j][glcx::Box::RIGHT];
          cube->colors[glcx::Box::RIGHT] = a[i][j][glcx::Box::TOP];
        }
        break;
      case glcx::Box::BACK:
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[j][2 - i][2]];
          for (int t = 0; t < 6; ++t) a[i][j][t] = cube->colors[t];
        }
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[i][j][2]];
          cube->colors[glcx::Box::FRONT] = a[i][j][glcx::Box::FRONT];
          cube->colors[glcx::Box::BACK] = a[i][j][glcx::Box::BACK];
          cube->colors[glcx::Box::TOP] = a[i][j][glcx::Box::RIGHT];
          cube->colors[glcx::Box::RIGHT] = a[i][j][glcx::Box::BOTTOM];
          cube->colors[glcx::Box::BOTTOM] = a[i][j][glcx::Box::LEFT];
          cube->colors[glcx::Box::LEFT] = a[i][j][glcx::Box::TOP];
        }
        break;
      case glcx::Box::LEFT:
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[2][j][2 - i]];
          for (int t = 0; t < 6; ++t) a[i][j][t] = cube->colors[t];
        }
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[2][i][j]];
          cube->colors[glcx::Box::LEFT] = a[i][j][glcx::Box::LEFT];
          cube->colors[glcx::Box::RIGHT] = a[i][j][glcx::Box::RIGHT];
          cube->colors[glcx::Box::TOP] = a[i][j][glcx::Box::BACK];
          cube->colors[glcx::Box::FRONT] = a[i][j][glcx::Box::TOP];
          cube->colors[glcx::Box::BOTTOM] = a[i][j][glcx::Box::FRONT];
          cube->colors[glcx::Box::BACK] = a[i][j][glcx::Box::BOTTOM];
        }
        break;
      case glcx::Box::RIGHT:
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[0][2 - j][i]];
          for (int t = 0; t < 6; ++t) a[i][j][t] = cube->colors[t];
        }
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[0][i][j]];
          cube->colors[glcx::Box::LEFT] = a[i][j][glcx::Box::LEFT];
          cube->colors[glcx::Box::RIGHT] = a[i][j][glcx::Box::RIGHT];
          cube->colors[glcx::Box::TOP] = a[i][j][glcx::Box::FRONT];
          cube->colors[glcx::Box::BACK] = a[i][j][glcx::Box::TOP];
          cube->colors[glcx::Box::BOTTOM] = a[i][j][glcx::Box::BACK];
          cube->colors[glcx::Box::FRONT] = a[i][j][glcx::Box::BOTTOM];
        }
        break;
      case glcx::Box::TOP:
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[2 - j][0][i]];
          for (int t = 0; t < 6; ++t) a[i][j][t] = cube->colors[t];
        }
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[i][0][j]];
          cube->colors[glcx::Box::TOP] = a[i][j][glcx::Box::TOP];
          cube->colors[glcx::Box::BOTTOM] = a[i][j][glcx::Box::BOTTOM];
          cube->colors[glcx::Box::FRONT] = a[i][j][glcx::Box::LEFT];
          cube->colors[glcx::Box::LEFT] = a[i][j][glcx::Box::BACK];
          cube->colors[glcx::Box::BACK] = a[i][j][glcx::Box::RIGHT];
          cube->colors[glcx::Box::RIGHT] = a[i][j][glcx::Box::FRONT];
        }
        break;
      case glcx::Box::BOTTOM:
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[j][2][2 - i]];
          for (int t = 0; t < 6; ++t) a[i][j][t] = cube->colors[t];
        }
        for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) {
          glcx::Box *cube = (glcx::Box*)subobjects[all[i][2][j]];
          cube->colors[glcx::Box::TOP] = a[i][j][glcx::Box::TOP];
          cube->colors[glcx::Box::BOTTOM] = a[i][j][glcx::Box::BOTTOM];
          cube->colors[glcx::Box::FRONT] = a[i][j][glcx::Box::RIGHT];
          cube->colors[glcx::Box::RIGHT] = a[i][j][glcx::Box::BACK];
          cube->colors[glcx::Box::BACK] = a[i][j][glcx::Box::LEFT];
          cube->colors[glcx::Box::LEFT] = a[i][j][glcx::Box::FRONT];
        }
        break;
    }
    front.clear(); back.clear(); left.clear(); right.clear(); top.clear(); bottom.clear();
    face.clear();
    for (int i = 0; i < 3; ++i) for (int j = 0; j < 3; ++j) for (int k = 0; k < 3; ++k)
      if ((i != 1) || (j != 1) || (k != 1)) {
        if (k == 0) {
          front.push_back(all[i][j][k]);
          face[all[i][j][k]] |= 1;
        }
        if (k == 2) {
          back.push_back(all[i][j][k]);
          face[all[i][j][k]] |= 2;
        }
        if (i == 2) {
          left.push_back(all[i][j][k]);
          face[all[i][j][k]] |= 4;
        }
        if (i == 0) {
          right.push_back(all[i][j][k]);
          face[all[i][j][k]] |= 8;
        }
        if (j == 0) {
          top.push_back(all[i][j][k]);
          face[all[i][j][k]] |= 16;
        }
        if (j == 2) {
          bottom.push_back(all[i][j][k]);
          face[all[i][j][k]] |= 32;
        }
      }
  }

  void render() {
    if (fabs(alpha - beta) < 0.1) {
      rotate();
      alpha = beta = 0.0;
      if (shuffling) {
        --shuffling;
        rotation = rand() % 6;
        beta = 90.0;
      } else rotation = -1;
    } else {
      alpha += 5.0;
      alpha = (alpha / 360.0 - floor(alpha / 360.0)) * 360.0;
    }
    for (glc::ObjectSet::iterator i = subobjects.begin(); i != subobjects.end(); ++i) {
      glMatrixMode(GL_MODELVIEW);
      glPushMatrix();
      if ((rotation != -1) && (face[i->second->id] & (1 << rotation))) {
        glRotatef(alpha, v[rotation].x, v[rotation].y, v[rotation].z);
      }
      glTranslatef(-i->second->position.x, -i->second->position.y, -i->second->position.z);
      i->second->render();
      glMatrixMode(GL_MODELVIEW);
      glPopMatrix();
    }
  }

  void renderForSelect() {
    for (glc::ObjectSet::iterator i = subobjects.begin(); i != subobjects.end(); ++i) {
      glMatrixMode(GL_MODELVIEW);
      glPushMatrix();
      glTranslatef(-i->second->position.x, -i->second->position.y, -i->second->position.z);
      i->second->renderForSelect();
      glMatrixMode(GL_MODELVIEW);
      glPopMatrix();
    }
  }

  void shuffle() {
    for (glc::ObjectSet::iterator i = subobjects.begin(); i != subobjects.end(); ++i) {
      glcx::Box *cube = (glcx::Box*)i->second;
      for (int t = 0; t < 6; ++t) cube->colors[t].a = 1.0;
    }
    shuffling = 20;
  }
};

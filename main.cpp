#include <windows.h>
#include <math.h>
#include <string.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\freeglut.h>

#include "RubiksCube.h"

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "freeglut.lib")

int w = 800, h = 600;
RubiksCube *rc = 0;
glc::ColorBuffer colorBuffer;
glcx::MenuItem *mi;

void Init() { 
  RubiksCubeParameters params;
  params.id = glc::genId();
  params.position.x = 0.0;
  params.position.y = 0.0;
  params.position.z = -3.0;
  params.size = 1.0;
  params.shuffle = true;
  rc = new RubiksCube(&params);

  glcx::MenuItemParameters mip;
  mip.id = glc::genId();
  mip.position.x = -1.0;
  mip.position.y = 1.0;
  mip.position.z = -3.0;
  mip.caption = "hello world";
  mip.action = 0;
  mi = new glcx::MenuItem(&mip);

  for (glc::ObjectSet::iterator i = rc->subobjects.begin(); i != rc->subobjects.end(); ++i) colorBuffer.addObject(i->second);

  glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glFlush();
}

void Render() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  if (rc->shuffling) {
    GLfloat matrix[4][4];
    glMatrixMode(GL_MODELVIEW);
    glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat*)matrix);
    float x = matrix[0][0] * 0.0 + matrix[0][1] * 1.0 + matrix[0][2] * 0.0;
    float y = matrix[1][0] * 0.0 + matrix[1][1] * 1.0 + matrix[1][2] * 0.0;
    float z = matrix[2][0] * 0.0 + matrix[2][1] * 1.0 + matrix[2][2] * 0.0;
    glRotatef(5.0, x, y, z);
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
//    mi->render();
    rc->render();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  glutSwapBuffers();
}

void Reshape(int width, int height) {
  w = width;
  h = height;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(65.0f, w/h, 1.0f, 1000.0f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(rc->position.x, rc->position.y, rc->position.z);
  glRotatef(30.0, 0.0, 1.0, 0.0);
  glRotatef(45.0, 1.0, 0.0, 0.0);
}

int Select(int x, int y) {
  glDisable(GL_LIGHTING);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
    rc->renderForSelect();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glEnable(GL_LIGHTING);
  unsigned char pixel[3];
  glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
  glc::Object *object = colorBuffer.getObject(pixel[0], pixel[1], pixel[2]);
  if (!object) return -1;
  return object->id;
}

int old_x, old_y;
bool dragging = false;

void MouseClick(int button, int state, int x, int y) {
  if (rc->shuffling) return;
  dragging = false;
  if (state == GLUT_DOWN) {
    if (button == GLUT_LEFT_BUTTON) {
      dragging = true;
      old_x = x; old_y = y;
    } else if (button == GLUT_RIGHT_BUTTON) {
      int n = Select(x, h - y);
      if (n >= 0) rc->click(n);
    } else if (button == GLUT_MIDDLE_BUTTON) rc->shuffle();
  }
}

void MouseMotion(int x, int y) {
  if (rc->shuffling) return;
  if (dragging) {
    float a = x - old_x, b = y - old_y;
    a = (a / 360.0 - floor(a / 360.0)) * 360.0; b = (b / 360.0 - floor(b / 360.0)) * 360.0;
    GLfloat matrix[4][4];
    glMatrixMode(GL_MODELVIEW);
    glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat*)matrix);
    float ax = matrix[0][0] * 0.0 + matrix[0][1] * 1.0 + matrix[0][2] * 0.0;
    float ay = matrix[1][0] * 0.0 + matrix[1][1] * 1.0 + matrix[1][2] * 0.0;
    float az = matrix[2][0] * 0.0 + matrix[2][1] * 1.0 + matrix[2][2] * 0.0;
    float bx = matrix[0][0] * 1.0 + matrix[0][1] * 0.0 + matrix[0][2] * 0.0;
    float by = matrix[1][0] * 1.0 + matrix[1][1] * 0.0 + matrix[1][2] * 0.0;
    float bz = matrix[2][0] * 1.0 + matrix[2][1] * 0.0 + matrix[2][2] * 0.0;
    glRotatef(a, ax, ay, az);
    glRotatef(b, bx, by, bz);
    old_x = x; old_y = y;
  }
}

void MousePassiveMotion(int x, int y) {
  if (rc->shuffling) return;
  int n = Select(x, h - y);
  if (n >= 0) rc->motion(n);
}

void Timer(int timeId) {
  glutPostRedisplay();
  glutTimerFunc(20, Timer, 0);
}

int main(int argc, char *argv[]) {
  glutInit(&argc, argv);
  glutInitWindowSize(800, 600);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("Rubick's Cube");
  glutDisplayFunc(Render);
  glutReshapeFunc(Reshape);
  Init();
  glutMouseFunc(MouseClick);
  glutMotionFunc(MouseMotion);
  glutPassiveMotionFunc(MousePassiveMotion);
  glutTimerFunc(10, Timer, 0);
  glutMainLoop();
  return 0;
}
